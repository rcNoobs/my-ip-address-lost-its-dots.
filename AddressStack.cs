﻿namespace ConsoleApplication1
{
    public class AddressStack
    {
        private string[] Addresses { get; set; }
        private int Top { get; set; }

        public AddressStack()
        {
            Addresses = new[]
            {
                "586130",
                "586240",
                "591672410",
                "10321176",
                "103211770",
                "1031012515",
                "1031012518",
                "1031012519",
                "108612270",
                "1501011352",
                "1501011353",
                "1501011354",
                "1501011355",
                "15010113512",
                "150101140197",
                "150101140198",
                "150101140199",
                "150101140200",
                "150101140215",
                "150101140216",
                "150101140217",
                "150101140218",
                "15010117264",
                "15010118364",
                "150101183160",
                "173199800",
                "173199810",
                "19223120324",
                "19223120325",
                "192231203130",
                "192231203131",
                "192231203145",
                "192231203146",
                "192231203163",
                "192231203166",
                "192231203169",
                "192231203175",
                "192231203177",
                "192231203192",
                "2026740",
                "2026141238",
                "2025514710",
                "20212517048",
                "203017832",
                "20321340",
                "20322180",
                "203141730",
                "20316214132",
                "20316214174",
                "20316214234",
                "20326940",
                "203341860",
                "20359270",
                "203173160",
                "203173170",
                "203173180",
                "203173200",
                "203173470",
                "203173500",
                "20317350143",
                "203173630",
                "203217240",
                "203217250",
                "2181004346",
                "21810043110"
            };

            Top = Addresses.Length - 1;
            VerifyAndResize();
        }

        public void Pop()
        {
            if (Top == -1) return;
            VerifyAndResize();
            Top--;
        }

        public void Push(string address)
        {
            VerifyAndResize();
            Addresses[++Top] = address;
        }

        public string Peek()
        {
            return Top != -1 ? Addresses[Top] : "Unknown";
        }

        public bool IsEmpty()
        {
            return Top == -1;
        }

        private void VerifyAndResize()
        {
            if (Top >= Addresses.Length / 2)
            {
                DoubleStackSize();
            }
            else if (Top <= Addresses.Length / 4)
            {
                HalfStackSize();
            }
        }

        private void DoubleStackSize()
        {
            var tmp = Addresses;
            Addresses = new string[Addresses.Length * 2];
            for (var i = 0; i < tmp.Length; i++)
            {
                Addresses[i] = tmp[i];
            }
        }

        private void HalfStackSize()
        {
            var tmp = Addresses;
            Addresses = new string[Addresses.Length / 2];
            for (var i = 0; i < Addresses.Length; i++)
            {
                Addresses[i] = tmp[i];
            }
        }
    }
}