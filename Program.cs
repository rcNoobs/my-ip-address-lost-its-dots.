﻿using System;
using ConsoleApplication1;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            AddressStack addr = new AddressStack();
            while (!addr.IsEmpty())
            {
                var a = addr.Peek();
                Console.WriteLine($"Validating string: {a}");
                AddressValidator.Validate(a).ForEach(Console.WriteLine);
                Console.WriteLine("%%%%%%%%%%%%%%%%%%%\n");
                addr.Pop();
            }
            Console.ReadLine();
        }
    }
}
