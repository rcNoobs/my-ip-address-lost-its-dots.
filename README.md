#Address Validator#
## I've got a list of IP Addresses without the dots, how do I get my address back? ##

Uses an array stack to hold a list of IP Addresses without the dots.

Pops each each entry and returns all the possible valid IP Addresses for that string. 

i.e.
Input 

```
#!c#


20212517048
```

output:

```
#!c#

202.125.17.048
202.125.170.48
```

Stack can grow/shrink dynamically to allow pushing addresses if you feel like implementing that =D