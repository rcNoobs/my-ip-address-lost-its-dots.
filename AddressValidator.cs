﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication1
{
    static class AddressValidator
    {
        public static List<string> Validate(string address)
        {
            if (address.Length < 4 || address.Length > 12)
                return null;

            var results = new List<string>();

            for (var f = 1; f <= 3; f++)
            {
                var fst = $"{address.Substring(0,f)}.{address.Substring(f)}";
                for (var s = f+1; s <= f+3; s++)
                {
                    var scnd = $"{fst.Substring(0, s+1)}.{fst.Substring(s + 1)}";
                    for (var t = s+1; t <= s+3; t++)
                    {
                        var trd = scnd.Length > t + 2 ?  $"{scnd.Substring(0, t+2)}.{scnd.Substring(t + 2)}" : string.Empty;
                        if (IsValidAddress(trd))
                            results.Add(trd);
                    }
                }
            }

            return results;
        }

        private static bool IsValidAddress(string address)
        {
            try
            {
                var tokens = address.Split('.');
                return tokens.Length == 4 && !tokens.Select(t => Convert.ToInt32(t)).Any(n => n < 0 || n > 255);
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
